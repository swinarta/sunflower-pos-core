package com.swinarta.sunflower.core.model;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
@Table(name="measurement")
public class Measurement extends AuditableObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4214000233249700399L;

	@NotEmpty(message="measurement.description")
	@Column(name="description", nullable=false)
	private String description;

	@NotEmpty(message="measurement.code")	
	@Column(name="code", nullable=false, length=3)
	private String code;
	
	@NotNull(message="measurement.qty")
	@Min(value=1)
	@Column(name="qty", nullable=false)
	private Integer defaultQty;
	
	private Boolean mutable;	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getDefaultQty() {
		return defaultQty;
	}

	public void setDefaultQty(Integer defaultQty) {
		this.defaultQty = defaultQty;
	}

	public Boolean getMutable() {
		return mutable;
	}

	public void setMutable(Boolean mutable) {
		this.mutable = mutable;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
