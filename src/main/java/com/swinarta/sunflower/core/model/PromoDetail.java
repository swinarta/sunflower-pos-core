package com.swinarta.sunflower.core.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="promo_product")
public class PromoDetail extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4596450952464817679L;

	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="product_id_no", nullable=false)
	private Product product;

	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="promo_id_no", nullable=false)
	private Promo promo;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Promo getPromo() {
		return promo;
	}

	public void setPromo(Promo promo) {
		this.promo = promo;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
