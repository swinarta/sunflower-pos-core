package com.swinarta.sunflower.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="transfer_order_detail")
public class TransferOrderDetail extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6881264639665856343L;

	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="product_id_no", nullable=false)
	private Product product;
	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="transfer_id_no", nullable=false)
	private TransferOrder transferOrder;
	
	@NotNull
	@Column(name="qty", nullable=false)
	private Float qty;
	
	public TransferOrder getTransferOrder() {
		return transferOrder;
	}

	public void setTransferOrder(TransferOrder transferOrder) {
		this.transferOrder = transferOrder;
	}

	public Float getQty() {
		return qty;
	}

	public void setQty(Float qty) {
		this.qty = qty;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
