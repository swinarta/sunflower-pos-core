package com.swinarta.sunflower.core.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

import org.apache.commons.lang.builder.ToStringBuilder;

@MappedSuperclass
public abstract class BaseNoIdObject implements Serializable{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 6230247283258214498L;

	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this);
	}	

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object obj);

}