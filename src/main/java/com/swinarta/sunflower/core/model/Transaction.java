package com.swinarta.sunflower.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="transaction")
public class Transaction extends BaseObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4810669578611062723L;

	@Column(name="station_id")
	private Integer stationId;
	
	@Column(name="transaction_dt")
	private Date transactionDate;
	
	@Column(name="total")
	private Long total;

	@Column(name="subtotal")
	private Long subtotal;

	@Column(name="disc_value")
	private Float discValue;

	@Column(name="disc_pct")
	private Float discPct;

	@Column(name="code")
	private String code;

	@Column(name="payment_type")
	private String paymentType;

	public Integer getStationId() {
		return stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Long subtotal) {
		this.subtotal = subtotal;
	}

	public Float getDiscValue() {
		return discValue;
	}

	public void setDiscValue(Float discValue) {
		this.discValue = discValue;
	}

	public Float getDiscPct() {
		return discPct;
	}

	public void setDiscPct(Float discPct) {
		this.discPct = discPct;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
