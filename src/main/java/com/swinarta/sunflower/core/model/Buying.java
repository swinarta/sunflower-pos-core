package com.swinarta.sunflower.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.swinarta.sunflower.core.util.PriceUtil;


@Entity
@Table(name="buying")
public class Buying extends AuditableNoIdObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8676902430778266673L;

	@Id
	@GeneratedValue(generator="foreign-generator")
	@GenericGenerator(name="foreign-generator", strategy="foreign", parameters={@Parameter(name="property",value="product")})	
	@Column(name="product_id_no", nullable=false)
	private Integer id;

	@DecimalMin(value="0.0")
	@NotNull
	@Column(name="buying_price", nullable=false, columnDefinition="double(12,2)")
	private Double buyingPrice;
	
	@DecimalMin(value="0.0")
	@NotNull
	@Column(name="disc1", nullable=false, columnDefinition="double(4,2)")
	private Double disc1;
	
	@DecimalMin(value="0.0")
	@NotNull
	@Column(name="disc2", nullable=false, columnDefinition="double(4,2)")
	private Double disc2;
	
	@DecimalMin(value="0.0")
	@NotNull
	@Column(name="disc3", nullable=false, columnDefinition="double(4,2)")
	private Double disc3;	
	
	@DecimalMin(value="0.0")
	@NotNull
	@Column(name="disc4", nullable=false, columnDefinition="double(4,2)")
	private Double disc4;
		
	@Column(name="incl_sales_tax", nullable=false)
	@NotNull
	private Boolean taxIncluded;
	
	@DecimalMin(value="0.0")
	@NotNull
	@Column(name="disc_price", nullable=false)
	private Double discPrice;
	
	@OneToOne
	@JoinColumn(name="supp_id_no", nullable=false)
	private Supplier supplier;

	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="measurement_id_no", nullable=false)
	private Measurement measurement;
	
	@Transient
	public Double getCostPrice() {
		return PriceUtil.getCostPrice(buyingPrice, disc1, disc2, disc3, disc4, discPrice, taxIncluded);
	}

	public Measurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Double getBuyingPrice() {
		return buyingPrice;
	}

	public void setBuyingPrice(Double buyingPrice) {
		this.buyingPrice = buyingPrice;
	}

	public Double getDisc1() {
		return disc1;
	}

	public void setDisc1(Double disc1) {
		this.disc1 = disc1;
	}

	public Double getDisc2() {
		return disc2;
	}

	public void setDisc2(Double disc2) {
		this.disc2 = disc2;
	}

	public Double getDisc3() {
		return disc3;
	}

	public void setDisc3(Double disc3) {
		this.disc3 = disc3;
	}

	public Double getDisc4() {
		return disc4;
	}

	public void setDisc4(Double disc4) {
		this.disc4 = disc4;
	}

	public Boolean getTaxIncluded() {
		return taxIncluded;
	}

	public void setTaxIncluded(Boolean taxIncluded) {
		this.taxIncluded = taxIncluded;
	}

	public Double getDiscPrice() {
		return discPrice;
	}

	public void setDiscPrice(Double discPrice) {
		this.discPrice = discPrice;
	}

	@OneToOne(mappedBy="buying", optional=false)
	private Product product;
		
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
