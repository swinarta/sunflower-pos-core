package com.swinarta.sunflower.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public abstract class AuditableObject extends BaseObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3275241876525731921L;	
	
	@Column(name="created_dt", nullable=false, updatable=false)
	private Date createdDt;

	@Column(name="updated_dt", nullable=false)
	private Date updatedDt;
	
	@Column(name="created_by", nullable=false, updatable=false)
	private Integer createdBy;
	
	@Column(name="updated_by", nullable=false)
	private Integer updatedBy;
	
	@Version
	@Column(name="version", nullable=false)
	private Integer version;
	
	@Column(name="delete_ind", nullable=false)
	private Boolean deleteInd;

	public Date getCreatedDt() {
		return createdDt;
	}

	public Date getUpdatedDt() {
		return updatedDt;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public Integer getVersion() {
		return version;
	}
	
	public void setVersion(Integer version) {
		this.version = version;
	}

	public Boolean getDeleteInd() {
		return deleteInd;
	}

	public void setDeleteInd(Boolean deleteInd) {
		this.deleteInd = deleteInd;
	}

}