package com.swinarta.sunflower.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="category")
public class CategoryWithMainCategory extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5221040816304010773L;

	@NotEmpty(message="category.description")
	@Column(name="description", nullable=false)
	private String description;
	
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=CategoryWithMainCategory.class, optional=true)
	@JoinColumn(name="main_category_id", nullable=true)
	private CategoryWithMainCategory mainCategory;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CategoryWithMainCategory getMainCategory() {
		if(mainCategory.getId() == 0) return null;
		return mainCategory;
	}

	public void setMainCategory(CategoryWithMainCategory mainCategory) {
		if(mainCategory.getId() == 0){
			this.mainCategory = null;
		}
		this.mainCategory = mainCategory;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
