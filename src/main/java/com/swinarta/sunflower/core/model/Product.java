package com.swinarta.sunflower.core.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="product")
public class Product extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7723851122395799069L;

	@NotEmpty(message="product.barcode")
	@Length(min=13, max=13, message="invalid.barcode")
	@Column(name="barcode", length=13, unique=true, nullable=false)
	private String barcode;
	
	@Length(min=5, max=8, message="invalid.sku")
	@NotEmpty(message="product.sku")
	@Column(name="sku", length=8, unique=true, nullable=false)
	private String sku;
	
	@NotNull(message="product.consignment")
	@Column(name="consignment", nullable=false)
	private Boolean consignment;
	
	@NotNull(message="product.scallable")
	@Column(name="scallable", nullable=false)
	private Boolean scallable;
	
	@NotEmpty(message="product.long.description")
	@Column(name="long_description", nullable=false)
	private String longDescription;
	
	@Column(name="short_description", length=24, nullable=false)
	private String shortDescription;
	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="cat_id", nullable=false)
	private Category category;
	
	@OneToOne(optional=true, fetch=FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private Buying buying;

	@OneToOne(optional=true, fetch=FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private Selling selling;

	@OneToMany(mappedBy="product", fetch=FetchType.LAZY)
	@MapKey(name="product_id_no")
	private Set<ProductMeasurement> productMeasurement;	
	
	@OneToMany(mappedBy="product", fetch=FetchType.LAZY)
	@MapKey(name="product_id_no")
	private Set<Stock> stock;
	
	public Set<ProductMeasurement> getProductMeasurement() {
		return productMeasurement;
	}

	public void setProductMeasurement(Set<ProductMeasurement> productMeasurement) {
		this.productMeasurement = productMeasurement;
	}

	public Set<Stock> getStock() {
		return stock;
	}

	public void setStock(Set<Stock> stock) {
		this.stock = stock;
	}

	/*	@OneToOne(cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private ProductImage image;

	public ProductImage getImage() {
		return image;
	}

	public void setImage(ProductImage image) {
		this.image = image;
	}
*/
	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Boolean getConsignment() {
		return consignment;
	}

	public void setConsignment(Boolean consignment) {
		this.consignment = consignment;
	}

	public Boolean getScallable() {
		return scallable;
	}

	public void setScallable(Boolean scallable) {
		this.scallable = scallable;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	public Buying getBuying() {
		return buying;
	}

	public void setBuying(Buying buying) {
		this.buying = buying;
	}

	public Selling getSelling() {
		return selling;
	}

	public void setSelling(Selling selling) {
		this.selling = selling;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}