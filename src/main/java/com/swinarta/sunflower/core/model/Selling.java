package com.swinarta.sunflower.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name="selling")
public class Selling extends AuditableNoIdObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2807373119837250597L;

	@Id
	@GeneratedValue(generator="foreign-generator")
	@GenericGenerator(name="foreign-generator", strategy="foreign", parameters={@Parameter(name="property",value="product")})	
	@Column(name="product_id_no", nullable=false)
	private Integer id;

	@DecimalMin(value="0.0")
	@NotNull
	@Column(name="selling_price", nullable=false)
	private Double sellingPrice;
	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="measurement_id_no", nullable=false)
	private Measurement measurement;
	
	public Measurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	@OneToOne(mappedBy="selling", optional=false)
	private Product product;
		
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
