package com.swinarta.sunflower.core.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="ro")
public class ReceivingOrder extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4456345659176746265L;

	@NotNull
	@Column(name="ro_id", nullable=false)
	private String roId;

	@NotNull
	@Column(name="ro_date", nullable=false)
	private Date roDate;

	@Column(name="remarks", nullable=true)
	private String remarks;

	@OneToOne
	@JoinColumn(name="po_id_no", nullable=false)
	private PurchasingOrder po;
	
	@Column(name= "status", columnDefinition="varchar", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="receivingOrder", cascade=CascadeType.ALL, orphanRemoval=true)
	private Set<ReceivingOrderDetail> receivingDetails;
			
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getRoId() {
		return roId;
	}

	public void setRoId(String roId) {
		this.roId = roId;
	}

	public Date getRoDate() {
		return roDate;
	}

	public void setRoDate(Date roDate) {
		this.roDate = roDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public PurchasingOrder getPo() {
		return po;
	}

	public void setPo(PurchasingOrder po) {
		this.po = po;
	}

	public Set<ReceivingOrderDetail> getReceivingDetails() {
		return receivingDetails;
	}

	public void setReceivingDetails(Set<ReceivingOrderDetail> receivingDetails) {
		this.receivingDetails = receivingDetails;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	public enum Status{

		NEW("NEW"),
		COMPLETED("COMPLETED");

		private String value;

		Status(String value) {
			this.value = value;
		}

		// the identifierMethod
		public String toString() {
			return value;
		}

		// the valueOfMethod
		public static Status fromString(String value) {
			if(value != null){
				if(value.equalsIgnoreCase("NEW")){
					return NEW;
				}else if(value.equalsIgnoreCase("COMPLETED")){
					return COMPLETED;
				}
			}
			
			return null;
		}
	}
	

}
