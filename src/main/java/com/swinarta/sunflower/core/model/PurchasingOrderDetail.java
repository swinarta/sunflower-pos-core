package com.swinarta.sunflower.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="po_detail")
public class PurchasingOrderDetail extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6881264639665856343L;

	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="product_id_no", nullable=false)
	private Product product;
	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="po_id_no", nullable=false)
	private PurchasingOrder purchasingOrder;
	
	@NotNull
	@Column(name="qty", nullable=false)
	private Float qty;
	
	@Column(name="cost_price_on_complete", nullable=true)
	private Double costPriceOnCompleted;

	@Column(name="on_buying_version", nullable=true)
	private Integer buyingVersionOnCompleted;
	
	public Integer getBuyingVersionOnCompleted() {
		return buyingVersionOnCompleted;
	}

	public void setBuyingVersionOnCompleted(Integer buyingVersionOnCompleted) {
		this.buyingVersionOnCompleted = buyingVersionOnCompleted;
	}

	public Double getCostPriceOnCompleted() {
		return costPriceOnCompleted;
	}

	public void setCostPriceOnCompleted(Double costPriceOnCompleted) {
		this.costPriceOnCompleted = costPriceOnCompleted;
	}

	public Float getQty() {
		return qty;
	}

	public void setQty(Float qty) {
		this.qty = qty;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public PurchasingOrder getPurchasingOrder() {
		return purchasingOrder;
	}

	public void setPurchasingOrder(PurchasingOrder purchasingOrder) {
		this.purchasingOrder = purchasingOrder;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
