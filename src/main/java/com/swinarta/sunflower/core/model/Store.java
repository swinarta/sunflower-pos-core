package com.swinarta.sunflower.core.model;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
@Table(name="store")
public class Store extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2228162757135479080L;

	@Column(name="code", length=5, unique=true, nullable=false)
	private String code;
	
	@Column(name="name", length=13, unique=true, nullable=false)
	private String name;

	@Column(name="address", nullable=false)
	private String address;

	@Column(name="city", nullable=false)
	private String city;

	@Column(name="is_store", nullable=false)
	private Boolean isStore;
	
	@Column(name="is_warehouse", nullable=false)
	private Boolean isWarehouse;
	
	@Column(name="is_hq", nullable=false)
	private Boolean isHq;

	@Column(name="remote_url", nullable=false)
	private String remoteURL;
	
	public String getRemoteURL() {
		return remoteURL;
	}
	public void setRemoteURL(String remoteURL) {
		this.remoteURL = remoteURL;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Boolean getIsHq() {
		return isHq;
	}
	public void setIsHq(Boolean isHq) {
		this.isHq = isHq;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getIsStore() {
		return isStore;
	}
	public void setIsStore(Boolean isStore) {
		this.isStore = isStore;
	}
	public Boolean getIsWarehouse() {
		return isWarehouse;
	}
	public void setIsWarehouse(Boolean isWarehouse) {
		this.isWarehouse = isWarehouse;
	}
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

}