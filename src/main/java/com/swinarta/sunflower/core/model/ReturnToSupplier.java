package com.swinarta.sunflower.core.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="return_to_supp")
public class ReturnToSupplier extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6513614052187281202L;

	@NotNull
	@Column(name="return_id", nullable=false)
	private String returnId;
	
	@NotNull
	@Column(name="return_date", nullable=false)
	private Date returnDate;
	
	@NotNull
	@Column(name="invoice_no", nullable=false)
	private String invoiceNumber;
	
	@Column(name="remarks", nullable=true)
	private String remarks;
	
	@Column(name= "status", columnDefinition="varchar", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;	
	
	@OneToOne
	@JoinColumn(name="supp_id_no", nullable=false)
	private Supplier supplier;
	
	@NotNull
	@Column(name="store_id_no", nullable=false)
	private Integer storeId;	
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="returnToSupplier", cascade=CascadeType.ALL)
	private Set<ReturnToSupplierDetail> returnDetails;
	
	public Set<ReturnToSupplierDetail> getReturnDetails() {
		return returnDetails;
	}
		
	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getReturnId() {
		return returnId;
	}

	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public enum Status{

		NEW("NEW"),
		CANCELLED("CANCELLED"),
		COMPLETED("COMPLETED");

		private String value;

		Status(String value) {
			this.value = value;
		}

		// the identifierMethod
		public String toString() {
			return value;
		}

		// the valueOfMethod
		public static Status fromString(String value) {
			if(value != null){
				if(value.equalsIgnoreCase("NEW")){
					return NEW;
				}else if(value.equalsIgnoreCase("COMPLETED")){
					return COMPLETED;
				}else if(value.equalsIgnoreCase("CANCELLED")){
					return CANCELLED;
				}
			}
			
			return null;
		}
	} 	

}