package com.swinarta.sunflower.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="product_measurement")
public class ProductMeasurement extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1074393805930628282L;

/*	@NotNull
	@Column(name="product_id_no", nullable=false)
	private Integer productId;
*/
	
	@ManyToOne
    @JoinColumn(name="product_id_no")
	private Product product;

	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="measurement_id_no", nullable=false)
	private Measurement measurement;
	
	@NotNull
	@Column(name="qty", nullable=false)
	private Integer overrideQty;		
	
/*	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}*/

	
	
	public Measurement getMeasurement() {
		return measurement;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}

	public Integer getOverrideQty() {
		return overrideQty;
	}

	public void setOverrideQty(Integer overrideQty) {
		this.overrideQty = overrideQty;
	}

	@Override
	public boolean equals(Object obj) {
		return false;
	}

	@Override
	public int hashCode() {
		return 0;
	}

}
