package com.swinarta.sunflower.core.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="po")
public class PurchasingOrder extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6513614052187281202L;

	@NotNull
	@Column(name="po_id", nullable=false)
	private String poId;
	
	@NotNull
	@Column(name="po_date", nullable=false)
	private Date poDate;
	
	@Column(name="remarks", nullable=true)
	private String remarks;
	
	@NotNull
	@Column(name="cancel_date", nullable=false)
	private Date cancelDate;

	@Column(name= "status", columnDefinition="varchar", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;	

	@NotNull
	@Column(name="deliver_store_id_no", nullable=false)
	private Integer deliverStoreId;

	@OneToOne
	@JoinColumn(name="supp_id_no", nullable=false)
	private Supplier supplier;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="purchasingOrder", cascade=CascadeType.ALL)
	private Set<PurchasingOrderDetail> orderDetails;
	
	public Set<PurchasingOrderDetail> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(Set<PurchasingOrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public String getPoId() {
		return poId;
	}

	public void setPoId(String poId) {
		this.poId = poId;
	}

	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getDeliverStoreId() {
		return deliverStoreId;
	}

	public void setDeliverStoreId(Integer deliverStoreId) {
		this.deliverStoreId = deliverStoreId;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public enum Status{

		NEW("NEW"),
		PROCESSED("PROCESSED"),
		CANCELLED("CANCELLED"),
		COMPLETED("COMPLETED");

		private String value;

		Status(String value) {
			this.value = value;
		}

		// the identifierMethod
		public String toString() {
			return value;
		}

		// the valueOfMethod
		public static Status fromString(String value) {
			if(value != null){
				if(value.equalsIgnoreCase("NEW")){
					return NEW;
				}else if(value.equalsIgnoreCase("PROCESSED")){
					return PROCESSED;
				}else if(value.equalsIgnoreCase("CANCELLED")){
					return CANCELLED;
				}else if(value.equalsIgnoreCase("COMPLETED")){
					return COMPLETED;
				}
			}
			
			return null;
		}
	}
 	

}
