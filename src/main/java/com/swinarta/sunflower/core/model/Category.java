package com.swinarta.sunflower.core.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="category")
public class Category extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5221040816304010773L;

	@NotEmpty(message="category.description")
	@Column(name="description", nullable=false)
	private String description;
		
	@ManyToOne(fetch=FetchType.LAZY, targetEntity=Category.class, optional=true)
	@JoinColumn(name="main_category_id", nullable=true)
	private Category mainCategory;

	@Valid
	@OneToMany(fetch=FetchType.LAZY, mappedBy="mainCategory")
	private Set<Category> subCategories;
	
	public Set<Category> getSubCategories() {
		return subCategories;
	}
	
	public void setSubCategories(Set<Category> subCategories) {
		this.subCategories = subCategories;
	}



	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Category getMainCategory() {
		return mainCategory;
	}

	public void setMainCategory(Category mainCategory) {
		this.mainCategory = mainCategory;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
