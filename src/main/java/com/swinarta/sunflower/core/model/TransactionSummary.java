package com.swinarta.sunflower.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="transaction")
public class TransactionSummary extends BaseObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8164354460684671027L;
	
	@Column(name="station_id")
	private Integer stationId;
	
	@Column(name="transaction_dt")
	private Date transactionDate;
	
	@Column(name="total")
	private Long total;
	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="created_by", nullable=false)
	private User user;
	
	@Column(name="payment_type")
	private String paymentType;

	public Integer getStationId() {
		return stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
