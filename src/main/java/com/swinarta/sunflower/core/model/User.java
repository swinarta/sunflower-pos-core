package com.swinarta.sunflower.core.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name="app_user")
public class User implements Serializable, UserDetails{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3676266113569756281L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Integer id;

	@Column(name="username", nullable=false)
	private String username;

	@Column(name="first_name", nullable=false)
	private String firstName;

	@Column(name="password", nullable=false)
	private String password;

	@Column(name="account_locked", nullable=false)
    private boolean accountLocked;
	
	@Column(name="account_enabled", nullable=false)
    private boolean enabled;

	@Column(name="account_expired", nullable=false)
    private boolean accountExpired;
	
	@Column(name="credentials_expired", nullable=false)
    private boolean credentialsExpired;

	@Transient
	private Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
	
    @ManyToMany(fetch = FetchType.EAGER) 
    @JoinTable(
            name="user_role",
            joinColumns = { @JoinColumn( name="user_id") },
            inverseJoinColumns = @JoinColumn( name="role_id")
    )
	private Set<Role> roles = new HashSet<Role>();

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
    public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public boolean isAccountLocked() {
		return accountLocked;
	}
	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}
	public Set<Role> getRoles() {
        return roles;
    }
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Override
	public int hashCode() {
		return 0;
	}
	@Override
	public boolean equals(Object obj) {
		return false;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public boolean isAccountExpired() {
		return accountExpired;
	}
	public boolean isCredentialsExpired() {
		return credentialsExpired;
	}
	public void setAccountExpired(boolean accountExpired) {
		this.accountExpired = accountExpired;
	}
	public void setCredentialsExpired(boolean credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}
	public Collection<GrantedAuthority> getAuthorities() {
		authorities.clear();
		for (Role role : roles) {
			authorities.add(role);
		}
		return authorities;
	}
	
	@Transient
	public boolean isAccountNonExpired() {
		return !accountExpired;
	}
	
	@Transient
	public boolean isAccountNonLocked() {		
		return !isAccountLocked();
	}
	
	@Transient
	public boolean isCredentialsNonExpired() {
		return !credentialsExpired;
	}
	
	public String toString(){
		return "username:"+username;
	}
}
