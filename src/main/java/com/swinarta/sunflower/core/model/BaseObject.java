package com.swinarta.sunflower.core.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseObject implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 5384600887707178783L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_no")
	private Integer id;
		
	/*
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this);
	}
	*/	

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object obj);

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}