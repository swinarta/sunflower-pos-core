package com.swinarta.sunflower.core.model;

public class ProductMeasurementBase {

	private String description;
	
	private Integer qty;
	
	private Boolean isDefault;
	
	private Integer id;
	
	private Boolean isMutable;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean getIsMutable() {
		return isMutable;
	}

	public void setIsMutable(Boolean isMutable) {
		this.isMutable = isMutable;
	}
	
}
