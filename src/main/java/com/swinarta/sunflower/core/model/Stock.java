package com.swinarta.sunflower.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="stock")
public class Stock extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3358032058980083017L;

	@ManyToOne
    @JoinColumn(name="product_id_no")
	private Product product;
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@NotNull
	@Column(name="max", nullable=false)
	private Integer max = 1;

	@NotNull
	@Column(name="min", nullable=false)
	private Integer min = 1;

	@NotNull
	@Column(name="default_order", nullable=false)
	private Integer defaultOrder = 1;

	@NotNull
	@Column(name="current", nullable=false)
	private Float current;

	@NotNull
	@Column(name="store_id_no", nullable=false)
	private Integer storeId;

	@Column(name= "last_update_reason", columnDefinition="varchar", nullable = false)
	@Enumerated(EnumType.STRING)
	private StockUpdateReason lastUpdateReason;	
	
	public Integer getStoreId() {
		return storeId;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	public Integer getMax() {
		return max;
	}

	public void setMax(Integer max) {
		this.max = max;
	}

	public Integer getMin() {
		return min;
	}

	public void setMin(Integer min) {
		this.min = min;
	}

	public Integer getDefaultOrder() {
		return defaultOrder;
	}

	public void setDefaultOrder(Integer defaultOrder) {
		this.defaultOrder = defaultOrder;
	}

	public Float getCurrent() {
		return current;
	}

	public void setCurrent(Float current) {
		this.current = current;
	}

	public StockUpdateReason getLastUpdateReason() {
		return lastUpdateReason;
	}

	public void setLastUpdateReason(StockUpdateReason lastUpdateReason) {
		this.lastUpdateReason = lastUpdateReason;
	}

	@Override
	public boolean equals(Object obj) {
		return false;
	}

	@Override
	public int hashCode() {
		return 0;
	}

	public enum StockUpdateReason{

		WEB("WEB"),
		SELL("SELL"),
		BUY("BUY"),
		RETSUP("RETSUP"),
		RETURN("RETURN"),
		TR_OUT("TR_OUT"),
		TR_OUT_C("TR_OUT_C"),
		TR_IN("TR_IN");

		private String value;

		StockUpdateReason(String value) {
			this.value = value;
		}

		// the identifierMethod
		public String toString() {
			return value;
		}

		// the valueOfMethod
		public  static StockUpdateReason fromString(String value) {
			if(value.equalsIgnoreCase("WEB")){
				return WEB;
			}else if(value.equalsIgnoreCase("SELL")){
				return SELL;
			}else if(value.equalsIgnoreCase("BUY")){
				return BUY;
			}else if(value.equalsIgnoreCase("RETSUP")){
				return RETSUP;
			}else if(value.equalsIgnoreCase("RETURN")){
				return RETURN;
			}else if(value.equalsIgnoreCase("TR_OUT")){
				return TR_OUT;
			}else if(value.equalsIgnoreCase("TR_OUT_C")){
				return TR_OUT_C;
			}else if(value.equalsIgnoreCase("TR_IN")){
				return TR_IN;
			}
			return null;
		}
	}

}
