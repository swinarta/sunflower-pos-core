package com.swinarta.sunflower.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="supplier")
public class Supplier extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1796947218635933661L;
	
	@NotEmpty(message="supplier.name")
	@Column(name="name", nullable=false)
	private String name;
	
	@NotEmpty(message="supplier.code")
	@Column(name="supplier_id", nullable=false, unique=true)
	private String supplierCode;
	
	@Column(name="address1", nullable=true)
	private String address1;

	@Column(name="address2", nullable=true)
	private String address2;
	
	@Column(name="city", nullable=true)
	private String city;

	@Column(name="phone", nullable=true)
	private String phone;
	
	@Column(name="fax", nullable=true)
	private String fax;
	
	@Column(name="mobile", nullable=true)
	private String mobile;

	@Column(name="contact_name", nullable=true)
	private String contactName;
	
	@NotNull(message="supplier.termofpayment")
	@Min(value=1)
	@Column(name="term_of_payment", nullable=false)
	private Integer termOfPayment;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public Integer getTermOfPayment() {
		return termOfPayment;
	}
	public void setTermOfPayment(Integer termOfPayment) {
		this.termOfPayment = termOfPayment;
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}