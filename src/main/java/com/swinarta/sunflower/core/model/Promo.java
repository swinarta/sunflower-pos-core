package com.swinarta.sunflower.core.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="promo")
public class Promo extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4611429574366314874L;

	@NotNull
	@Column(name="start_dt", nullable=false)
	private Date startDate;
	
	@NotNull
	@Column(name="end_dt", nullable=false)
	private Date endDate;
		
	@Column(name= "promo_type", columnDefinition="varchar", nullable = false)
	@Enumerated(EnumType.STRING)
	private PromoType promoType;
	
	@NotNull
	@Column(name="promo_value", nullable=false)
	private Double promoValue;

	@NotNull
	@Column(name="qty", nullable=false)
	private Integer qty = 1;

	@Column(name="description", nullable=true)
	private String description;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="promo")
	private Set<PromoDetail> promoDetails;
	
	public Set<PromoDetail> getPromoDetails() {
		return promoDetails;
	}

	public void setPromoDetails(Set<PromoDetail> promoDetails) {
		this.promoDetails = promoDetails;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public PromoType getPromoType() {
		return promoType;
	}

	public void setPromoType(PromoType promoType) {
		this.promoType = promoType;
	}

	public Double getPromoValue() {
		return promoValue;
	}

	public void setPromoValue(Double promoValue) {
		this.promoValue = promoValue;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public enum PromoType{

		VALUE("VALUE"),
		PCT("PCT");

		private String value;

		PromoType(String value) {
			this.value = value;
		}

		// the identifierMethod
		public String toString() {
			return value;
		}

		// the valueOfMethod
		public  static PromoType fromString(String value) {
			if(value.equalsIgnoreCase("VALUE")){
				return VALUE;
			}else if(value.equalsIgnoreCase("PCT")){
				return PCT;
			}
			
			return null;
		}
	}

}
