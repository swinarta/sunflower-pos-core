package com.swinarta.sunflower.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="transfer_order")
public class TransferOrder extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4611429574366314874L;

	@NotNull
	@Column(name="transfer_dt", nullable=false)
	private Date transferDate;

	@Column(name="from_store_code", nullable=true)
	private String fromStoreCode;

	@Column(name="to_store_code", nullable=true)
	private String toStoreCode;

	@Column(name="remarks", nullable=true)
	private String remarks;	

	@Column(name= "status", columnDefinition="varchar", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;	
	
	@NotNull
	@Column(name="transfer_id", nullable=false)
	private String transferId;
	
	public String getTransferId() {
		return transferId;
	}

	public void setTransferId(String transferId) {
		this.transferId = transferId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	public String getFromStoreCode() {
		return fromStoreCode;
	}

	public void setFromStoreCode(String fromStoreCode) {
		this.fromStoreCode = fromStoreCode;
	}

	public String getToStoreCode() {
		return toStoreCode;
	}

	public void setToStoreCode(String toStoreCode) {
		this.toStoreCode = toStoreCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/*	@OneToMany(fetch=FetchType.LAZY, mappedBy="promo")
	private Set<PromoDetail> promoDetails;
	
	public Set<PromoDetail> getPromoDetails() {
		return promoDetails;
	}

	public void setPromoDetails(Set<PromoDetail> promoDetails) {
		this.promoDetails = promoDetails;
	}
*/
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

	public enum Status{

		NEW("NEW"),
		PROCESSED("PROCESSED"),
		CANCELLED("CANCELLED"),
		COMPLETED("COMPLETED");

		private String value;

		Status(String value) {
			this.value = value;
		}

		// the identifierMethod
		public String toString() {
			return value;
		}

		// the valueOfMethod
		public static Status fromString(String value) {
			if(value != null){
				if(value.equalsIgnoreCase("NEW")){
					return NEW;
				}else if(value.equalsIgnoreCase("PROCESSED")){
					return PROCESSED;
				}else if(value.equalsIgnoreCase("CANCELLED")){
					return CANCELLED;
				}else if(value.equalsIgnoreCase("COMPLETED")){
					return COMPLETED;
				}
			}
			
			return null;
		}
	}

}
