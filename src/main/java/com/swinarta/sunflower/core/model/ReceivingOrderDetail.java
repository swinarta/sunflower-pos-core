package com.swinarta.sunflower.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="ro_detail")
public class ReceivingOrderDetail extends AuditableObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3408032586332473813L;

	@NotNull
	@Column(name="qty", nullable=true)
	private Float qty = 0f;
	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="ro_id_no", nullable=false)
	private ReceivingOrder receivingOrder;
	
	@OneToOne
	@JoinColumn(name="po_detail_id_no", nullable=false)
	private PurchasingOrderDetail poDetail;
	
	public PurchasingOrderDetail getPoDetail() {
		return poDetail;
	}

	public void setPoDetail(PurchasingOrderDetail poDetail) {
		this.poDetail = poDetail;
	}

	public ReceivingOrder getReceivingOrder() {
		return receivingOrder;
	}

	public void setReceivingOrder(ReceivingOrder receivingOrder) {
		this.receivingOrder = receivingOrder;
	}

	public Float getQty() {
		return qty;
	}

	public void setQty(Float qty) {
		this.qty = qty;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return false;
	}

}
