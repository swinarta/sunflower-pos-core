package com.swinarta.sunflower.core.dao.hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.swinarta.sunflower.core.dao.UniversalDao;
import com.swinarta.sunflower.core.data.ResultList;

public class UniversalDaoImpl extends HibernateDaoSupport implements UniversalDao{

	private Validator validator;
	
	public void setValidator(Validator validator) {
		this.validator = validator;
	}

	public <T> T get(Class<T> clazz, Serializable id) {
        Object o = getHibernateTemplate().get(clazz, id);

        if (o == null) {
            throw new ObjectRetrievalFailureException(clazz, id);
        }

        return clazz.cast(o);
	}

	public Object save(Object o) throws Exception {
				
		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(o);
		
		if(!constraintViolations.isEmpty()){
			//throw new ErrorCodeException(ErrorCodeConstants.getDefaultInstance().getINVALIDOBJECTDETAILS(), ValidatorUtils.convertIntoString(constraintViolations));
			throw new Exception("hibernate validation error " + constraintViolations);
		}		
		
    	getHibernateTemplate().saveOrUpdate(o);
        return o;
	}
	
	public <T> T save(Class<T> clazz, Object o) throws Exception{

		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(o);
		
		if(!constraintViolations.isEmpty()){
			//throw new ErrorCodeException(ErrorCodeConstants.getDefaultInstance().getINVALIDOBJECTDETAILS(), ValidatorUtils.convertIntoString(constraintViolations));
			throw new Exception("hibernate validation error " + constraintViolations);
		}		

		getHibernateTemplate().saveOrUpdate(o);
		return clazz.cast(o);
	}
	
	public void remove(Object o){
		getHibernateTemplate().delete(o);
	}

	public <T> Collection<T> getAll(Class<T> clazz) {
		return getAll(clazz, false);
	}

	public <T> Collection<T> getAll(Class<T> clazz, boolean useCache) {

		getHibernateTemplate().setCacheQueries(useCache);

		if(useCache){
			getHibernateTemplate().setQueryCacheRegion("query.getAll." + clazz.getName());
		}

		return getHibernateTemplate().loadAll(clazz);		
	}

	public <T> ResultList<T> getAllResultList(Class<T> clazz) {
		return getAllResultList(clazz, null);
	}
	
	public <T> ResultList<T> getAllResultList(Class<T> clazz, String orderBy){
		return getAllResultList(clazz, orderBy, false);
	}
	
	public <T> ResultList<T> getAllResultList(Class<T> clazz, String orderBy, boolean useCache) {
		ArrayList<T> list = new ArrayList<T>();
		String query = "select obj from " + clazz.getName() + " obj";
		if(orderBy != null){
			query += " order by " + orderBy;
		}
		
		Collection retList = getHibernateTemplate().find(query);
		for (Object object : retList) {			
			list.add(clazz.cast(object));
		}

		return new ResultList<T>(list);
	}

}
