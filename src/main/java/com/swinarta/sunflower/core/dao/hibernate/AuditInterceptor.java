package com.swinarta.sunflower.core.dao.hibernate;

import java.io.Serializable;
import java.util.Date;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.swinarta.sunflower.core.model.AuditableNoIdObject;
import com.swinarta.sunflower.core.model.AuditableObject;
import com.swinarta.sunflower.core.model.User;

public class AuditInterceptor extends EmptyInterceptor{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1744430684335776685L;

	private Integer getActorId(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	Integer actorId = 999;
    	
    	if(auth == null){
    		System.out.println("no authentication found");
    		System.out.println("use 999 as default user id");
    	}else{
    		User actor = (User)auth.getPrincipal();
        	actorId = actor.getId();
    	}
    	
    	return actorId;
		
	}
	
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		if(entity instanceof AuditableObject || entity instanceof AuditableNoIdObject){

            for ( int i=0; i<propertyNames.length; i++ ) {
                if ( "createdDt".equals( propertyNames[i] ) ) {
                    state[i] = new Date();
                }else if("updatedDt".equals(propertyNames[i])){
                	state[i] = new Date();
                }else if("createdBy".equals(propertyNames[i])){
                	state[i] = getActorId();
                }else if("updatedBy".equals(propertyNames[i])){
                	state[i] = getActorId();                
                }else if("deleteInd".equals(propertyNames[i])){
                	state[i] = false;
                }
            }
            return true;
		}
		return false;
	}

	public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
		if(entity instanceof AuditableObject || entity instanceof AuditableNoIdObject){
            for ( int i=0; i<propertyNames.length; i++ ) {
                if("updatedDt".equals(propertyNames[i])){
                	currentState[i] = new Date();
                }else if("updatedBy".equals(propertyNames[i])){
                	currentState[i] = getActorId();
                }
            }
			
			return true;
		}
		return false;
	}

}
