package com.swinarta.sunflower.core.dao.hibernate;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.swinarta.sunflower.core.dao.CoreDao;
import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.model.Buying;
import com.swinarta.sunflower.core.model.Category;
import com.swinarta.sunflower.core.model.CategoryWithMainCategory;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.Promo;
import com.swinarta.sunflower.core.model.PromoDetail;
import com.swinarta.sunflower.core.model.PurchasingOrder;
import com.swinarta.sunflower.core.model.PurchasingOrderDetail;
import com.swinarta.sunflower.core.model.ReceivingOrder;
import com.swinarta.sunflower.core.model.ReceivingOrderDetail;
import com.swinarta.sunflower.core.model.ReturnToSupplier;
import com.swinarta.sunflower.core.model.ReturnToSupplierDetail;
import com.swinarta.sunflower.core.model.Selling;
import com.swinarta.sunflower.core.model.Stock;
import com.swinarta.sunflower.core.model.Store;
import com.swinarta.sunflower.core.model.Supplier;
import com.swinarta.sunflower.core.model.Transaction;
import com.swinarta.sunflower.core.model.TransactionSummary;
import com.swinarta.sunflower.core.model.TransferOrder;
import com.swinarta.sunflower.core.model.TransferOrderDetail;
import com.swinarta.sunflower.core.model.User;
import com.swinarta.sunflower.core.model.PurchasingOrder.Status;

public class CoreDaoImpl extends HibernateDaoSupport implements CoreDao{

	public static Integer DEFAULT_START = 0;
	public static Integer DEFAULT_END = 1;
	
	@SuppressWarnings("unchecked")
	private Integer countTotalResult(DetachedCriteria dc){		
		dc.setProjection(Projections.rowCount());
		Integer count = ((Number) DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(dc, 0, 1))).intValue();

		dc.setProjection(null);
		return count;
	}

	@SuppressWarnings("unchecked")
	public User getUserByUserName(String username){
		DetachedCriteria dc = DetachedCriteria.forClass(User.class);
		dc.add(Restrictions.eq("username", username));
		return (User)DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(dc));
	}
	
	@SuppressWarnings("unchecked")
	public ResultList<Supplier> searchSupplier(String text, Integer start, Integer end) {
		DetachedCriteria dc = DetachedCriteria.forClass(Supplier.class);
		
		dc.add(Restrictions.or(Restrictions.like("name", text + "%"), Restrictions.like("supplierCode", text + "%")));
				
		dc.addOrder(Order.asc("name"));

		ResultList<Supplier> result =  new ResultList<Supplier>(getHibernateTemplate().findByCriteria(dc, start, end));		
		result.setTotalCount(countTotalResult(dc));
		result.setStartRow(start);
		result.setEndRow(end);
		return result;
	}

	
	@SuppressWarnings("unchecked")
	public Category getCategory(Integer categoryId){
		DetachedCriteria dc = DetachedCriteria.forClass(Category.class);
		dc.add(Restrictions.eq("id", categoryId));
		dc.setFetchMode("mainCategory", FetchMode.JOIN);
		dc.setFetchMode("mainCategory.mainCategory", FetchMode.JOIN);
		
		List<Category> findByCriteria = (List<Category>)getHibernateTemplate().findByCriteria(dc);
		return DataAccessUtils.uniqueResult(findByCriteria);		
	}
	
	@SuppressWarnings("unchecked")
	public ResultList<Product> searchProduct(Integer supplierId, String barcode, String sku, String description, Boolean deleteInd, Boolean hasSelling, Integer start, Integer end) {
		if(start == null){
			start = DEFAULT_START;
		}
		if(end == null){
			end = DEFAULT_END;
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(Product.class);
		
		if(barcode != null){
			dc.add(Restrictions.eq("barcode", barcode));
		}
		
		if(sku != null){
			dc.add(Restrictions.eq("sku", sku));
		}

		if(deleteInd != null){
			dc.add(Restrictions.eq("deleteInd", deleteInd));
		}

		if(StringUtils.isNotEmpty(description)){
			dc.add(Restrictions.like("longDescription", description + "%"));
		}
		
		if(supplierId != null && supplierId > 0){
			dc.createAlias("buying", "buying", Criteria.INNER_JOIN);
			dc.add(Restrictions.eq("buying.supplier.id", supplierId));			
		}else{
			dc.createAlias("buying", "buying", Criteria.LEFT_JOIN);
		}
		
		if(hasSelling != null && hasSelling){
			dc.createAlias("selling", "selling", Criteria.INNER_JOIN);
		}else{
			dc.createAlias("selling", "selling", Criteria.LEFT_JOIN);
		}
		
		dc.setFetchMode("category.mainCategory", FetchMode.JOIN);
		dc.setFetchMode("category.mainCategory.mainCategory", FetchMode.JOIN);
		dc.addOrder(Order.asc("longDescription"));

		ResultList<Product> result =  new ResultList<Product>(getHibernateTemplate().findByCriteria(dc, start, end));		
		result.setTotalCount(countTotalResult(dc));
		result.setStartRow(start);
		result.setEndRow(end);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public ResultList<Product> searchRelatedProduct(Integer productId, Integer categoryId, String description, Integer start, Integer end){
		
		if(start == null){
			start = DEFAULT_START;
		}
		if(end == null){
			end = DEFAULT_END;
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(Product.class);
		dc.add(Restrictions.ne("id", productId));
		dc.add(Restrictions.eq("category.id", categoryId));

		if(StringUtils.isNotEmpty(description)){
			dc.add(Restrictions.like("longDescription", description + "%"));
		}
				
		dc.addOrder(Order.asc("longDescription"));

		dc.setFetchMode("category.mainCategory", FetchMode.JOIN);
		dc.setFetchMode("category.mainCategory.mainCategory", FetchMode.JOIN);

		ResultList<Product> result =  new ResultList<Product>(getHibernateTemplate().findByCriteria(dc, start, end));		
		result.setTotalCount(countTotalResult(dc));
		result.setStartRow(start);
		result.setEndRow(end);
		return result;
	}

	@SuppressWarnings("unchecked")
	public ResultList<Product> searchProductWithStock(Integer supplierId, String barcode, String sku, String description, Boolean deleteInd, Integer storeId, Integer stockMode, Integer start, Integer end) {
		if(start == null){
			start = DEFAULT_START;
		}
		if(end == null){
			end = DEFAULT_END;
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(Product.class);
		
		if(barcode != null){
			dc.add(Restrictions.eq("barcode", barcode));
		}
		
		if(sku != null){
			dc.add(Restrictions.eq("sku", sku));
		}

		if(deleteInd != null){
			dc.add(Restrictions.eq("deleteInd", deleteInd));
		}

		if(StringUtils.isNotEmpty(description)){
			dc.add(Restrictions.like("longDescription", description + "%"));
		}
		
		if(supplierId != null && supplierId > 0){
			dc.createAlias("buying", "buying", Criteria.INNER_JOIN);
			dc.add(Restrictions.eq("buying.supplier.id", supplierId));			
		}else{
			dc.createAlias("buying", "buying", Criteria.LEFT_JOIN);
		}
		
		DetachedCriteria stockdc = dc.createCriteria("stock", "stck", Criteria.LEFT_JOIN, Restrictions.eq("stck.storeId", storeId));
		
		if(stockMode != null){
			if(stockMode == 1){
				stockdc.add(Restrictions.leProperty("current", "min"));
			}else if(stockMode == 2){
				stockdc.add(Restrictions.geProperty("current", "max"));
			}
		}
				
		dc.addOrder(Order.asc("longDescription"));
				
		ResultList<Product> result =  new ResultList<Product>(getHibernateTemplate().findByCriteria(dc, start, end));		
		result.setTotalCount(countTotalResult(dc));
		result.setStartRow(start);
		result.setEndRow(end);
		return result;
	}

	@SuppressWarnings("unchecked")
	public ProductMeasurement getProductMeasurement(Integer productId, Integer measurementId){
		DetachedCriteria dc = DetachedCriteria.forClass(ProductMeasurement.class);
		dc.add(Restrictions.eq("product.id", productId));
		dc.add(Restrictions.eq("measurement.id", measurementId));
		List<ProductMeasurement> findByCriteria = (List<ProductMeasurement>)getHibernateTemplate().findByCriteria(dc);
		return DataAccessUtils.uniqueResult(findByCriteria);
	}
	
	@SuppressWarnings("unchecked")
	public ResultList<ProductMeasurement> searchProductMeasurement(Integer productId){
		DetachedCriteria dc = DetachedCriteria.forClass(ProductMeasurement.class);
		dc.add(Restrictions.eq("product.id", productId));
		ResultList<ProductMeasurement> result =  new ResultList<ProductMeasurement>(getHibernateTemplate().findByCriteria(dc));
		result.setStartRow(0);
		result.setEndRow(result.size()-1);
		result.setTotalCount(result.size());
		return result;
	}

	@SuppressWarnings("unchecked")
	public ResultList<PurchasingOrder> searchPurchasingOrder(String text, Integer supplierId, Status status, Integer start, Integer end){
		DetachedCriteria dc = DetachedCriteria.forClass(PurchasingOrder.class);
		
		if(StringUtils.isNotEmpty(text)){
			dc.add(Restrictions.eq("poId", text));
		}
		
		if(supplierId != null && supplierId > 0){
			dc.add(Restrictions.eq("supplier.id", supplierId));
		}
		
		if(status != null){
			dc.add(Restrictions.eq("status", status));
		}
		
		dc.addOrder(Order.desc("poDate"));

		ResultList<PurchasingOrder> result =  new ResultList<PurchasingOrder>(getHibernateTemplate().findByCriteria(dc, start, end));		
		result.setTotalCount(countTotalResult(dc));
		result.setStartRow(start);
		result.setEndRow(end);
		return result;
	}

	@SuppressWarnings("unchecked")
	public Stock getStock(Integer productId, Integer storeId){
		DetachedCriteria dc = DetachedCriteria.forClass(Stock.class);
		dc.add(Restrictions.eq("product.id", productId));
		dc.add(Restrictions.eq("storeId", storeId));
		List<Stock> findByCriteria = (List<Stock>)getHibernateTemplate().findByCriteria(dc);
		return DataAccessUtils.uniqueResult(findByCriteria);
	}
		
	@SuppressWarnings("unchecked")
	public ResultList<CategoryWithMainCategory> getAllCategory(String orderBy){
		DetachedCriteria dc = DetachedCriteria.forClass(CategoryWithMainCategory.class);
		dc.add(Restrictions.isNotNull("mainCategory"));
		dc.add(Restrictions.ne("mainCategory.id", 0));
		dc.addOrder(Order.asc(orderBy));
		ResultList<CategoryWithMainCategory> result =  new ResultList<CategoryWithMainCategory>(getHibernateTemplate().findByCriteria(dc));
		result.setStartRow(0);
		result.setEndRow(result.size()-1);
		result.setTotalCount(result.size());
		return result;
	}

	public ResultList<CategoryWithMainCategory> getAllCategory(){
		return getAllCategory("description");
	}
	
	@SuppressWarnings("unchecked")
	public String getNextSku(){
		DetachedCriteria dc = DetachedCriteria.forClass(Product.class);
		dc.add(Restrictions.like("sku", "886%"));
		dc.setProjection(Projections.max("sku"));
		List<String> findByCriteria = getHibernateTemplate().findByCriteria(dc);
		String retval =  DataAccessUtils.uniqueResult(findByCriteria);
		Integer retvalInt = Integer.parseInt(retval) + 1;
		return String.valueOf(retvalInt);
	}
	
	@SuppressWarnings("unchecked")
	public List<PurchasingOrderDetail> getPurchasingOrderDetails(Integer poId){
		
		DetachedCriteria dc = DetachedCriteria.forClass(Buying.class);
		dc.add(Restrictions.eqProperty("product.id", "p.id"));
		dc.setProjection(Projections.property("measurement.id"));
		
		DetachedCriteria dcpo = DetachedCriteria.forClass(PurchasingOrder.class);
		dcpo.add(Restrictions.eq("id", poId));
		dcpo.setProjection(Projections.property("deliverStoreId"));
		
		DetachedCriteria c = DetachedCriteria.forClass(PurchasingOrderDetail.class);
		c.add(Restrictions.eq("purchasingOrder.id", poId));

		c.createAlias("product", "p");
		c.createAlias("product.buying", "buying", Criteria.INNER_JOIN);
		c.createAlias("product.selling", "selling", Criteria.INNER_JOIN);
		c.createAlias("p.buying.measurement", "bmeasurement");
		c.createAlias("product.productMeasurement", "pm", Criteria.LEFT_JOIN, Subqueries.propertyEq("measurement.id", dc));
		c.createAlias("product.stock", "stck", Criteria.LEFT_JOIN, Subqueries.propertyEq("stck.storeId", dcpo));
		
		c.addOrder(Order.asc("p.longDescription"));
		
		return getHibernateTemplate().findByCriteria(c);
		
	}
	
	@SuppressWarnings("unchecked")
	public PurchasingOrderDetail getPurchasingOrderDetail(Integer detailId){
		DetachedCriteria dc = DetachedCriteria.forClass(Buying.class);
		dc.add(Restrictions.eqProperty("product.id", "p.id"));
		dc.setProjection(Projections.property("measurement.id"));
		
		DetachedCriteria dcpo = DetachedCriteria.forClass(PurchasingOrder.class);
		dcpo.add(Restrictions.eqProperty("id", "po.id"));
		dcpo.setProjection(Projections.property("deliverStoreId"));
		
		DetachedCriteria c = DetachedCriteria.forClass(PurchasingOrderDetail.class);
		c.add(Restrictions.eq("id", detailId));

		c.createAlias("product", "p");
		c.setFetchMode("product.buying", FetchMode.JOIN);
		c.setFetchMode("product.selling", FetchMode.JOIN);
		c.createAlias("product.buying", "buying", Criteria.INNER_JOIN);
		c.createAlias("product.selling", "selling", Criteria.INNER_JOIN);
		c.createAlias("p.buying.measurement", "bmeasurement");
		c.createAlias("purchasingOrder", "po");
		c.createAlias("product.productMeasurement", "pm", Criteria.LEFT_JOIN, Subqueries.propertyEq("measurement.id", dc));
		c.createAlias("product.stock", "stck", Criteria.LEFT_JOIN, Subqueries.propertyEq("stck.storeId", dcpo));
		
		return (PurchasingOrderDetail)DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(c));
	}

	
	@SuppressWarnings("unchecked")
	public ReceivingOrder getReceivingOrderByPO(Integer poId){
		DetachedCriteria dc = DetachedCriteria.forClass(ReceivingOrder.class);
		dc.add(Restrictions.eq("po.id", poId));
		return (ReceivingOrder)DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(dc));
	}

	@SuppressWarnings("unchecked")
	public ReceivingOrder getReceivingOrderByCode(String code){		
		DetachedCriteria dc = DetachedCriteria.forClass(ReceivingOrder.class);
		if(code.startsWith("P")){
			dc.createAlias("po", "pord");
			dc.add(Restrictions.eq("pord.poId", code));
		}else{
			dc.add(Restrictions.eq("roId", code));
		}
		return (ReceivingOrder)DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(dc));
	}

	@SuppressWarnings("unchecked")
	public List<ReceivingOrderDetail> getReceivingOrderDetails(Integer roId){
		
		DetachedCriteria dc = DetachedCriteria.forClass(Buying.class);
		dc.add(Restrictions.eqProperty("product.id", "prod.id"));
		dc.setProjection(Projections.property("measurement.id"));
		
		DetachedCriteria c = DetachedCriteria.forClass(ReceivingOrderDetail.class);
		c.add(Restrictions.eq("receivingOrder.id", roId));
		c.createAlias("poDetail.product", "prod");
		c.createAlias("prod.category", "category");
		c.createAlias("prod.buying", "buying");
		c.createAlias("prod.selling", "selling", Criteria.LEFT_JOIN);
		c.createAlias("buying.supplier", "supp");
		c.createAlias("buying.measurement", "bmeasurement");
		c.createAlias("prod.productMeasurement", "pm", Criteria.LEFT_JOIN, Subqueries.propertyEq("measurement.id", dc));
		
		c.addOrder(Order.asc("prod.longDescription"));

		return getHibernateTemplate().findByCriteria(c);
	}
	
	@SuppressWarnings("unchecked")
	public ReceivingOrderDetail getReceivingOrderDetail(Integer detailId){
		DetachedCriteria dc = DetachedCriteria.forClass(Buying.class);
		dc.add(Restrictions.eqProperty("product.id", "prod.id"));
		dc.setProjection(Projections.property("measurement.id"));

		DetachedCriteria c = DetachedCriteria.forClass(ReceivingOrderDetail.class);
		c.add(Restrictions.eq("id", detailId));
		
		c.createAlias("poDetail.product", "prod");
		c.createAlias("prod.category", "category");
		c.createAlias("prod.buying", "buying");
		c.createAlias("prod.selling", "selling", Criteria.LEFT_JOIN);
		c.createAlias("buying.supplier", "supp");
		c.createAlias("buying.measurement", "bmeasurement");
		c.createAlias("prod.productMeasurement", "pm", Criteria.LEFT_JOIN, Subqueries.propertyEq("measurement.id", dc));
		
		return (ReceivingOrderDetail)DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(c));
	}
	
	@SuppressWarnings("unchecked")
	public List<Promo> getCurrentAndFuturePromo(){
		Date today = DateUtils.truncate(Calendar.getInstance(), Calendar.DATE).getTime();
		DetachedCriteria dc = DetachedCriteria.forClass(Promo.class);		
		dc.add(Restrictions.ge("endDate", today));
		
		dc.addOrder(Order.asc("startDate"));
		dc.addOrder(Order.asc("endDate"));
		
		return getHibernateTemplate().findByCriteria(dc);
	}

	@SuppressWarnings("unchecked")
	public Promo getPromo(Integer productId, Date startDate, Date endDate){
				
		DetachedCriteria dc = DetachedCriteria.forClass(Promo.class);
		dc.add(Restrictions.or(
				Restrictions.or(
					Restrictions.between("startDate", startDate, endDate), 
					Restrictions.between("endDate", startDate, endDate))
				,
				Restrictions.and(
						Restrictions.lt("startDate", startDate), 
						Restrictions.gt("endDate", endDate))
		));
		dc.createAlias("promoDetails", "details");
		dc.add(Restrictions.eq("details.product.id", productId));
		
		return (Promo)DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(dc));
	}
	
	@SuppressWarnings("unchecked")
	public List<PromoDetail> getPromoDetails(Integer promoId){
		DetachedCriteria dc = DetachedCriteria.forClass(Selling.class);
		dc.add(Restrictions.eqProperty("product.id", "p.id"));
		dc.setProjection(Projections.property("measurement.id"));
		
		DetachedCriteria c = DetachedCriteria.forClass(PromoDetail.class);
		c.add(Restrictions.eq("promo.id", promoId));
		c.createAlias("product", "p");
		c.createAlias("p.selling", "s");
		c.createAlias("product.productMeasurement", "pm", Criteria.LEFT_JOIN, Subqueries.propertyEq("measurement.id", dc));
		c.createAlias("product.buying", "b", Criteria.LEFT_JOIN);
		c.createAlias("product.buying.supplier", "supp", Criteria.LEFT_JOIN);
		
		c.addOrder(Order.asc("p.longDescription"));
		
		return getHibernateTemplate().findByCriteria(c);

	}
	
	@SuppressWarnings("unchecked")
	public PromoDetail getPromoDetail(Integer detailId){
		DetachedCriteria dc = DetachedCriteria.forClass(Selling.class);
		dc.add(Restrictions.eqProperty("product.id", "p.id"));
		dc.setProjection(Projections.property("measurement.id"));

		DetachedCriteria c = DetachedCriteria.forClass(PromoDetail.class);
		c.add(Restrictions.eq("id", detailId));
		c.createAlias("product", "p");
		c.createAlias("p.selling", "s");		
		c.createAlias("product.productMeasurement", "pm", Criteria.LEFT_JOIN, Subqueries.propertyEq("measurement.id", dc));
		c.createAlias("product.buying", "b", Criteria.LEFT_JOIN);
		c.createAlias("product.buying.supplier", "supp", Criteria.LEFT_JOIN);
		
		return (PromoDetail)DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(c));
	}

	@SuppressWarnings("unchecked")
	public List<Object> findTransactionSummaryByStation(Date startDate, Date endDate){
		DetachedCriteria dc = DetachedCriteria.forClass(TransactionSummary.class);
		dc.setProjection(Projections.projectionList()
			    .add(Projections.sum("total").as("total"))
			    .add(Projections.groupProperty("stationId").as("stationId"))
			    .add(Projections.count("id").as("id"))
			    );
		dc.add(Restrictions.between("transactionDate", startDate, endDate));
		
		return getHibernateTemplate().findByCriteria(dc);
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> findTransactionSummaryByPaymentType(Date startDate, Date endDate){
		DetachedCriteria dc = DetachedCriteria.forClass(TransactionSummary.class);
		dc.createAlias("user", "u",Criteria.LEFT_JOIN);
		dc.setFetchMode("user", FetchMode.JOIN);
		dc.setProjection(Projections.projectionList()
			    .add(Projections.sum("total").as("total"))
			    .add(Projections.count("id").as("id"))			    
			    .add(Projections.groupProperty("stationId").as("stationId"))
			    .add(Projections.groupProperty("paymentType").as("paymentType"))
			    .add(Projections.groupProperty("u.id"))
			    .add(Projections.property("u.firstName"))
			    );
		dc.add(Restrictions.between("transactionDate", startDate, endDate));
		
		return getHibernateTemplate().findByCriteria(dc);
		
	}

	@SuppressWarnings("unchecked")
	public ResultList<ReturnToSupplier> searchReturnToSupplier(String text, Integer supplierId, com.swinarta.sunflower.core.model.ReturnToSupplier.Status status, String invoiceNumber, Integer start, Integer end){
		DetachedCriteria dc = DetachedCriteria.forClass(ReturnToSupplier.class);
		
		if(StringUtils.isNotEmpty(text)){
			dc.add(Restrictions.eq("returnId", text));
		}

		if(StringUtils.isNotEmpty(invoiceNumber)){
			dc.add(Restrictions.eq("invoiceNumber", invoiceNumber));
		}

		if(supplierId != null && supplierId > 0){
			dc.add(Restrictions.eq("supplier.id", supplierId));
		}
		
		if(status != null){
			dc.add(Restrictions.eq("status", status));
		}
		
		dc.addOrder(Order.desc("returnDate"));

		ResultList<ReturnToSupplier> result =  new ResultList<ReturnToSupplier>(getHibernateTemplate().findByCriteria(dc, start, end));		
		result.setTotalCount(countTotalResult(dc));
		result.setStartRow(start);
		result.setEndRow(end);
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<ReturnToSupplierDetail> getReturnToSupplierDetails(Integer retId){
		
		DetachedCriteria dc = DetachedCriteria.forClass(Buying.class);
		dc.add(Restrictions.eqProperty("product.id", "p.id"));
		dc.setProjection(Projections.property("measurement.id"));
		
		DetachedCriteria dcpo = DetachedCriteria.forClass(ReturnToSupplier.class);
		dcpo.add(Restrictions.eq("id", retId));
		dcpo.setProjection(Projections.property("storeId"));
		
		DetachedCriteria c = DetachedCriteria.forClass(ReturnToSupplierDetail.class);
		c.add(Restrictions.eq("returnToSupplier.id", retId));

		c.createAlias("product", "p");
		c.createAlias("p.buying", "b");
		c.createAlias("p.buying.measurement", "bmeasurement");
		c.createAlias("product.productMeasurement", "pm", Criteria.LEFT_JOIN, Subqueries.propertyEq("measurement.id", dc));
		c.createAlias("product.stock", "stck", Criteria.LEFT_JOIN, Subqueries.propertyEq("stck.storeId", dcpo));
		
		c.addOrder(Order.asc("p.longDescription"));
		
		return getHibernateTemplate().findByCriteria(c);
		
	}

	@SuppressWarnings("unchecked")
	public ReturnToSupplierDetail getReturnToSupplierDetail(Integer detailId){
		DetachedCriteria dc = DetachedCriteria.forClass(Buying.class);
		dc.add(Restrictions.eqProperty("product.id", "p.id"));
		dc.setProjection(Projections.property("measurement.id"));
				
		DetachedCriteria c = DetachedCriteria.forClass(ReturnToSupplierDetail.class);
		c.add(Restrictions.eq("id", detailId));

		c.createAlias("product", "p");
		c.createAlias("p.buying", "b");
		c.createAlias("p.buying.measurement", "bmeasurement");
		c.createAlias("returnToSupplier", "ret");
		c.createAlias("product.productMeasurement", "pm", Criteria.LEFT_JOIN, Subqueries.propertyEq("measurement.id", dc));
		
		return (ReturnToSupplierDetail)DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(c));
	}

	@SuppressWarnings("unchecked")
	public ResultList<Transaction> searchTransaction(Date startDate, Date endDate, Integer stationId, String code, Integer start, Integer end){
		if(start == null){
			start = DEFAULT_START;
		}
		if(end == null){
			end = DEFAULT_END;
		}
		
		DetachedCriteria dc = DetachedCriteria.forClass(Transaction.class);
		
		if(stationId != null){
			dc.add(Restrictions.eq("stationId", stationId));
		}
		
		if(code != null){
			dc.add(Restrictions.eq("code", code));
		}

		dc.add(Restrictions.between("transactionDate", startDate, endDate));
		
		ResultList<Transaction> result =  new ResultList<Transaction>(getHibernateTemplate().findByCriteria(dc, start, end));		
		result.setTotalCount(countTotalResult(dc));
		result.setStartRow(start);
		result.setEndRow(end);
		return result;
		
	}

	@SuppressWarnings("unchecked")
	public Store getStore(String code){
		DetachedCriteria dc = DetachedCriteria.forClass(Store.class);
		dc.add(Restrictions.eq("code", code));
		List<Store> findByCriteria = (List<Store>)getHibernateTemplate().findByCriteria(dc);
		return DataAccessUtils.uniqueResult(findByCriteria);
	}

	@SuppressWarnings("unchecked")
	public ResultList<TransferOrder> searchTransferOrder(com.swinarta.sunflower.core.model.TransferOrder.Status status, Integer start, Integer end){
		DetachedCriteria dc = DetachedCriteria.forClass(TransferOrder.class);
		
		if(status != null){
			dc.add(Restrictions.eq("status", status));
		}
		
		dc.addOrder(Order.desc("transferDate"));

		ResultList<TransferOrder> result =  new ResultList<TransferOrder>(getHibernateTemplate().findByCriteria(dc, start, end));		
		result.setTotalCount(countTotalResult(dc));
		result.setStartRow(start);
		result.setEndRow(end);
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TransferOrderDetail> getTransferOrderDetails(Integer transferId){
		
		DetachedCriteria dc = DetachedCriteria.forClass(Buying.class);
		dc.add(Restrictions.eqProperty("product.id", "p.id"));
		dc.setProjection(Projections.property("measurement.id"));
		
		DetachedCriteria c = DetachedCriteria.forClass(TransferOrderDetail.class);
		c.add(Restrictions.eq("transferOrder.id", transferId));

		c.createAlias("product", "p");
		c.createAlias("product.buying", "buying", Criteria.INNER_JOIN);
		c.createAlias("p.buying.measurement", "bmeasurement");
		c.createAlias("product.productMeasurement", "pm", Criteria.LEFT_JOIN, Subqueries.propertyEq("measurement.id", dc));		
		
		c.addOrder(Order.asc("p.longDescription"));
		
		return getHibernateTemplate().findByCriteria(c);
		
	}

	@SuppressWarnings("unchecked")
	public TransferOrderDetail getTransferOrderDetail(Integer detailId){
		DetachedCriteria dc = DetachedCriteria.forClass(Buying.class);
		dc.add(Restrictions.eqProperty("product.id", "p.id"));
		dc.setProjection(Projections.property("measurement.id"));
				
		DetachedCriteria c = DetachedCriteria.forClass(TransferOrderDetail.class);
		c.add(Restrictions.eq("id", detailId));

		c.createAlias("product", "p");
		c.setFetchMode("product.buying", FetchMode.JOIN);
		c.createAlias("product.buying", "buying", Criteria.INNER_JOIN);
		c.createAlias("p.buying.measurement", "bmeasurement");
		c.createAlias("transferOrder", "to");
		c.createAlias("product.productMeasurement", "pm", Criteria.LEFT_JOIN, Subqueries.propertyEq("measurement.id", dc));
		
		return (TransferOrderDetail)DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(c));
	}

	
}
