package com.swinarta.sunflower.core.dao;

import java.util.Date;
import java.util.List;

import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.model.Category;
import com.swinarta.sunflower.core.model.CategoryWithMainCategory;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.Promo;
import com.swinarta.sunflower.core.model.PromoDetail;
import com.swinarta.sunflower.core.model.PurchasingOrder;
import com.swinarta.sunflower.core.model.PurchasingOrderDetail;
import com.swinarta.sunflower.core.model.ReceivingOrder;
import com.swinarta.sunflower.core.model.ReceivingOrderDetail;
import com.swinarta.sunflower.core.model.ReturnToSupplier;
import com.swinarta.sunflower.core.model.ReturnToSupplierDetail;
import com.swinarta.sunflower.core.model.Store;
import com.swinarta.sunflower.core.model.Transaction;
import com.swinarta.sunflower.core.model.TransferOrder;
import com.swinarta.sunflower.core.model.TransferOrderDetail;
import com.swinarta.sunflower.core.model.User;
import com.swinarta.sunflower.core.model.PurchasingOrder.Status;
import com.swinarta.sunflower.core.model.Stock;
import com.swinarta.sunflower.core.model.Supplier;

public interface CoreDao {

	public User getUserByUserName(String username);
	
	public ResultList<Supplier> searchSupplier(String text, Integer start, Integer end);
	
	public ResultList<Product> searchProduct(Integer supplierId, String barcode, String sku, String description, Boolean deleteInd, Boolean hasSelling, Integer start, Integer end);
	public ResultList<Product> searchRelatedProduct(Integer productId, Integer categoryId, String description, Integer start, Integer end);
	public ResultList<Product> searchProductWithStock(Integer supplierId, String barcode, String sku, String description, Boolean deleteInd, Integer storeId, Integer stockMode, Integer start, Integer end);
	
	public ResultList<ProductMeasurement> searchProductMeasurement(Integer productId);
	public ResultList<PurchasingOrder> searchPurchasingOrder(String text, Integer supplierId, Status status, Integer start, Integer end);
	public Stock getStock(Integer productId, Integer storeId);
	public ProductMeasurement getProductMeasurement(Integer productId, Integer measurementId);
	
	public Category getCategory(Integer categoryId);
	public ResultList<CategoryWithMainCategory> getAllCategory();
	public ResultList<CategoryWithMainCategory> getAllCategory(String orderBy);
	
	public String getNextSku();
	
	public List<PurchasingOrderDetail> getPurchasingOrderDetails(Integer poId);
	public PurchasingOrderDetail getPurchasingOrderDetail(Integer detailId);
	
	public ReceivingOrder getReceivingOrderByPO(Integer poId);
	public ReceivingOrder getReceivingOrderByCode(String code);
	public List<ReceivingOrderDetail> getReceivingOrderDetails(Integer roId);
	public ReceivingOrderDetail getReceivingOrderDetail(Integer detailId);
	
	public ResultList<ReturnToSupplier> searchReturnToSupplier(String text, Integer supplierId, com.swinarta.sunflower.core.model.ReturnToSupplier.Status status, String invoiceNumber, Integer start, Integer end);
	public List<ReturnToSupplierDetail> getReturnToSupplierDetails(Integer retId);
	public ReturnToSupplierDetail getReturnToSupplierDetail(Integer detailId);
	
	public List<Promo> getCurrentAndFuturePromo();
	public Promo getPromo(Integer productId, Date startDate, Date endDate);
	
	public List<PromoDetail> getPromoDetails(Integer promoId);
	public PromoDetail getPromoDetail(Integer detailId);
	
	public List<Object> findTransactionSummaryByStation(Date startDate, Date endDate);
	public List<Object> findTransactionSummaryByPaymentType(Date startDate, Date endDate);
	
	public ResultList<Transaction> searchTransaction(Date startDate, Date endDate, Integer stationId, String code, Integer start, Integer end);
	
	public Store getStore(String code);
	
	public ResultList<TransferOrder> searchTransferOrder(com.swinarta.sunflower.core.model.TransferOrder.Status status, Integer start, Integer end);
	public List<TransferOrderDetail> getTransferOrderDetails(Integer transferId);
	public TransferOrderDetail getTransferOrderDetail(Integer detailId);
}
