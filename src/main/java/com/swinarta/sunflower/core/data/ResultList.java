package com.swinarta.sunflower.core.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

public class ResultList<T> extends ArrayList<T> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -25443057318674786L;
	
	public ResultList(ResultList<?> list, Class<T> clazz){
		
		for (Object obj : list) {
			Object newInstance;
			try {
				newInstance = clazz.newInstance();
				BeanUtils.copyProperties(obj, newInstance);
				add(clazz.cast(newInstance));
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		startRow = list.startRow;
		endRow = list.endRow;
		totalCount = list.totalCount;
		
	}
		
	public ResultList(List<T> list){
		super(list);
		startRow = 0;
		totalCount = size();
	}
	
	public boolean add(T e){		
		boolean retval = super.add(e);
		totalCount = size();
		endRow = totalCount - 1;
		return retval;
	}
	
	private int totalCount;
	private int startRow;
	private int endRow;
	
	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
		
}
