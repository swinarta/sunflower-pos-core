package com.swinarta.sunflower.core.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.time.DateUtils;

public class DateUtil {

	public static Date getMinimumDateTime(Date date){
		return DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
	}

	public static Date getMaximumDateTime(Date date){
		GregorianCalendar gcal = new GregorianCalendar();
		Calendar cal = Calendar.getInstance();
		cal.setTime(getMinimumDateTime(date));
		cal.set(Calendar.HOUR, gcal.getMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, gcal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, gcal.getMaximum(Calendar.SECOND));
		return cal.getTime();
	}
	
}
