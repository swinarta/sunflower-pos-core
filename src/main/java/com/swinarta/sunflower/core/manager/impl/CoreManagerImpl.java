package com.swinarta.sunflower.core.manager.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.RestTemplate;

import com.swinarta.sunflower.core.dao.CoreDao;
import com.swinarta.sunflower.core.dao.UniversalDao;
import com.swinarta.sunflower.core.data.ResultList;
import com.swinarta.sunflower.core.manager.CoreManager;
import com.swinarta.sunflower.core.model.Category;
import com.swinarta.sunflower.core.model.CategoryWithMainCategory;
import com.swinarta.sunflower.core.model.Measurement;
import com.swinarta.sunflower.core.model.Product;
import com.swinarta.sunflower.core.model.ProductMeasurement;
import com.swinarta.sunflower.core.model.Promo;
import com.swinarta.sunflower.core.model.PromoDetail;
import com.swinarta.sunflower.core.model.PurchasingOrder;
import com.swinarta.sunflower.core.model.ReturnToSupplier;
import com.swinarta.sunflower.core.model.ReturnToSupplierDetail;
import com.swinarta.sunflower.core.model.Store;
import com.swinarta.sunflower.core.model.Transaction;
import com.swinarta.sunflower.core.model.TransferOrder;
import com.swinarta.sunflower.core.model.TransferOrderDetail;
import com.swinarta.sunflower.core.model.PurchasingOrder.Status;
import com.swinarta.sunflower.core.model.PurchasingOrderDetail;
import com.swinarta.sunflower.core.model.ReceivingOrder;
import com.swinarta.sunflower.core.model.ReceivingOrderDetail;
import com.swinarta.sunflower.core.model.Stock;
import com.swinarta.sunflower.core.model.Stock.StockUpdateReason;
import com.swinarta.sunflower.core.model.Supplier;
import com.swinarta.sunflower.core.model.User;
import com.swinarta.sunflower.core.util.DateUtil;

public class CoreManagerImpl implements CoreManager{
	
	private UniversalDao universalDao;
	
	private CoreDao coreDao;
	
	private RestTemplate restTemplate;

	private Properties properties;

	public void setProperties(Properties properties) {
		this.properties = properties;
	}
	
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public void setUniversalDao(UniversalDao universalDao) {
		this.universalDao = universalDao;
	}

	public void setCoreDao(CoreDao coreDao) {
		this.coreDao = coreDao;
	}

	public <T> T get(Class<T> clazz, Serializable id){
		return universalDao.get(clazz, id);
	}
	
	public <T> T save(Class<T> clazz, Object o) throws Exception {
		return universalDao.save(clazz, o);
	}
	
	public void remove(Object o){
		universalDao.remove(o);
	}
	
	public User getUserByUserName(String username){
		return coreDao.getUserByUserName(username);
	}
	
	public Collection<Measurement> getAllMeasurement(){
		return universalDao.getAll(Measurement.class);
	}

	public Collection<Store> getAllStore(){
		return universalDao.getAll(Store.class);
	}

	public ResultList<Supplier> getAllSupplier(){
		return universalDao.getAllResultList(Supplier.class, "name");
	}

	public ResultList<CategoryWithMainCategory> getAllCategory(){
		return coreDao.getAllCategory();
	}
	
	public Category getCategory(Integer categoryId){
		return coreDao.getCategory(categoryId);
	}

	public ResultList<Supplier> searchSupplier(String text, Integer start, Integer end){
		return coreDao.searchSupplier(text, start, end);
	}
	
	public ResultList<Product> searchProduct(Integer supplierId, String barcode, String sku, String description, Boolean deleteInd, Boolean hasSelling, Integer start, Integer end){
		return coreDao.searchProduct(supplierId, barcode, sku, description, deleteInd, hasSelling, start, end);
	}
	
	public ResultList<Product> searchRelatedProduct(Integer productId, Integer categoryId, String description, Integer start, Integer end){
		return coreDao.searchRelatedProduct(productId, categoryId, description, start, end);
	}
	
	public ResultList<Product> searchProductWithStock(Integer supplierId, String barcode, String sku, String description, Boolean deleteInd, Integer storeId, Integer stockMode, Integer start, Integer end){
		return coreDao.searchProductWithStock(supplierId, barcode, sku, description, deleteInd, storeId, stockMode, start, end);
	}
	
	public ResultList<ProductMeasurement> searchProductMeasurement(Integer productId){
		return coreDao.searchProductMeasurement(productId);
	}
	
	public ResultList<PurchasingOrder> searchPurchasingOrder(String text, Integer supplierId, Status status, Integer start, Integer end){
		return coreDao.searchPurchasingOrder(text, supplierId, status, start, end);
	}
	
	public ResultList<ReturnToSupplier> searchReturnToSupplier(String text, Integer supplierId, com.swinarta.sunflower.core.model.ReturnToSupplier.Status status, String invoiceNumber, Integer start, Integer end){
		return coreDao.searchReturnToSupplier(text, supplierId, status, invoiceNumber, start, end);
	}
	
	public List<ReturnToSupplierDetail> getReturnToSupplierDetails(Integer retId){
		return coreDao.getReturnToSupplierDetails(retId);
	}
	
	public ReturnToSupplierDetail getReturnToSupplierDetail(Integer detailId){
		return coreDao.getReturnToSupplierDetail(detailId);
	}

	public ReturnToSupplier updateCompleteReturnToSupplier(Integer retId) throws Exception{
		ReturnToSupplier ret = universalDao.get(ReturnToSupplier.class, retId);
		ret.setStatus(com.swinarta.sunflower.core.model.ReturnToSupplier.Status.COMPLETED);
				
		List<ReturnToSupplierDetail> returnDetails = coreDao.getReturnToSupplierDetails(retId); 
		for (ReturnToSupplierDetail detail : returnDetails) {

			Integer measurementQty;
			Set<ProductMeasurement> pms =  detail.getProduct().getProductMeasurement();
			if(pms.isEmpty()){
				measurementQty = detail.getProduct().getBuying().getMeasurement().getDefaultQty();
			}else{
				measurementQty = pms.iterator().next().getOverrideQty();
			}
			
			detail.setBuyingVersionOnCompleted(detail.getProduct().getBuying().getVersion());
			detail.setCostPriceOnCompleted(detail.getProduct().getBuying().getCostPrice());
			
			Float returnQty = detail.getQty();
			Float qtyToDel = -1 * returnQty * measurementQty;
			
			Set<Stock> stocks = detail.getProduct().getStock();
			Stock stock;
			if(stocks.isEmpty()){
				stock = new Stock();
				stock.setCurrent(qtyToDel);
				stock.setStoreId(ret.getStoreId());
				stock.setProduct(detail.getProduct());
			}else{
				stock = stocks.iterator().next();
				stock.setCurrent(stock.getCurrent() + qtyToDel);
			}
			stock.setLastUpdateReason(StockUpdateReason.RETSUP);
			universalDao.save(stock);		
		}		
		
		return (ReturnToSupplier) universalDao.save(ret);
	}

	public Stock getStock(Integer productId, Integer storeId){
		return coreDao.getStock(productId, storeId);
	}
	
	public ProductMeasurement getProductMeasurement(Integer productId, Integer measurementId){
		return coreDao.getProductMeasurement(productId, measurementId);
	}
	
	public String getNextSku(){
		return coreDao.getNextSku();
	}
	
	public List<PurchasingOrderDetail> getPurchasingOrderDetails(Integer poId){
		return coreDao.getPurchasingOrderDetails(poId);
		
/*		PurchasingOrder po = universalDao.get(PurchasingOrder.class, poId);
		Set<PurchasingOrderDetail> details = po.getOrderDetails();
		List<PurchasingOrderDetail> result = new ArrayList<PurchasingOrderDetail>();
		for (PurchasingOrderDetail purchasingOrderDetail : details) {
			result.add(purchasingOrderDetail);
		}
		return result;
*/		
	}
	
	public PurchasingOrderDetail getPurchasingOrderDetail(Integer detailId){
		return coreDao.getPurchasingOrderDetail(detailId);
	}
	
	public PurchasingOrder updateCancelPurchasingOrder(Integer poId) throws Exception{
		
		PurchasingOrder po = universalDao.get(PurchasingOrder.class, poId);
		po.setStatus(Status.CANCELLED);
		
		//delete RO associate with this PO
		ReceivingOrder currRo = getReceivingOrderByPO(poId);
		if(currRo != null){
			remove(currRo);
		}
		return save(PurchasingOrder.class, po);
		
	}
	
	public List<Promo> getCurrentAndFuturePromo(){
		return coreDao.getCurrentAndFuturePromo();
	}
	
	public Promo getPromo(Integer productId, Date startDate, Date endDate){
		return coreDao.getPromo(productId, startDate, endDate);
	}

	public List<PromoDetail> getPromoDetails(Integer promoId) {
		return coreDao.getPromoDetails(promoId);
	}

	public PromoDetail getPromoDetail(Integer detailId) {		
		return coreDao.getPromoDetail(detailId);
	}
	
	public ReceivingOrder createReceivingOrder(Integer poId, Date roDate) throws Exception{
		
		PurchasingOrder po = universalDao.get(PurchasingOrder.class, poId);
		
		ReceivingOrder ro = new ReceivingOrder();
		ro.setRoDate(roDate);
		ro.setStatus(com.swinarta.sunflower.core.model.ReceivingOrder.Status.NEW);
		ro.setPo(po);
		ro.setRoId("temp");
		
		Set<PurchasingOrderDetail> poDetails = po.getOrderDetails();
		
		Set<ReceivingOrderDetail> roDetails = new HashSet<ReceivingOrderDetail>();
		
		for (PurchasingOrderDetail poDetail : poDetails) {
			ReceivingOrderDetail roDetail = new ReceivingOrderDetail();
			roDetail.setPoDetail(poDetail);
			roDetail.setReceivingOrder(ro);
			roDetails.add(roDetail);
		}
		
		ro.setReceivingDetails(roDetails);
		
		return (ReceivingOrder)universalDao.save(ro);
		
	}
	
	public ReceivingOrder getReceivingOrderByCode(String code){
		return coreDao.getReceivingOrderByCode(code);
	}
	
	public ReceivingOrder getReceivingOrderByPO(Integer poId){
		return coreDao.getReceivingOrderByPO(poId);
	}
	
	public List<ReceivingOrderDetail> getReceivingOrderDetails(Integer roId){
		return coreDao.getReceivingOrderDetails(roId);
	}
	
	public ReceivingOrderDetail getReceivingOrderDetail(Integer detailId){
		return coreDao.getReceivingOrderDetail(detailId);
	}
	
	public ReceivingOrder updateCompleteReceivingOrder(Integer roId) throws Exception{
		ReceivingOrder ro = universalDao.get(ReceivingOrder.class, roId);
		ro.setStatus(com.swinarta.sunflower.core.model.ReceivingOrder.Status.COMPLETED);
		PurchasingOrder po = ro.getPo();
		po.setStatus(Status.COMPLETED);
		
		Map<Integer, Float> receivingOrderMap = new HashMap<Integer, Float>();
		
		List<ReceivingOrderDetail> receivingDetails = coreDao.getReceivingOrderDetails(ro.getId());
		for (ReceivingOrderDetail receivingOrderDetail : receivingDetails) {
			receivingOrderMap.put(receivingOrderDetail.getPoDetail().getId(), receivingOrderDetail.getQty());
		}
		
		List<PurchasingOrderDetail> orderDetails = coreDao.getPurchasingOrderDetails(po.getId()); 
		for (PurchasingOrderDetail purchasingOrderDetail : orderDetails) {
			purchasingOrderDetail.setBuyingVersionOnCompleted(purchasingOrderDetail.getProduct().getBuying().getVersion());
			purchasingOrderDetail.setCostPriceOnCompleted(purchasingOrderDetail.getProduct().getBuying().getCostPrice());

			Integer measurementQty;
			Set<ProductMeasurement> pms =  purchasingOrderDetail.getProduct().getProductMeasurement();
			if(pms.isEmpty()){
				measurementQty = purchasingOrderDetail.getProduct().getBuying().getMeasurement().getDefaultQty();
			}else{
				measurementQty = pms.iterator().next().getOverrideQty();
			}
			
			Float receiveQty = receivingOrderMap.get(purchasingOrderDetail.getId());
			Float qtyToAdd = receiveQty * measurementQty;
			
			Set<Stock> stocks = purchasingOrderDetail.getProduct().getStock();
			Stock stock;
			if(stocks.isEmpty()){
				stock = new Stock();
				stock.setCurrent(qtyToAdd);
				stock.setStoreId(purchasingOrderDetail.getPurchasingOrder().getDeliverStoreId());
				stock.setProduct(purchasingOrderDetail.getProduct());
			}else{
				stock = stocks.iterator().next();
				stock.setCurrent(stock.getCurrent() + qtyToAdd);
			}
			stock.setLastUpdateReason(StockUpdateReason.BUY);
			universalDao.save(stock);		
		}		
		
		ro.setPo(universalDao.save(PurchasingOrder.class, po));
		return (ReceivingOrder) universalDao.save(ro);
	}
	
	public List<Object> findTransactionSummaryByStation(Date date){
		return coreDao.findTransactionSummaryByStation(DateUtil.getMinimumDateTime(date), DateUtil.getMaximumDateTime(date));
	}

	public List<Object> findTransactionSummaryByPaymentType(Date date){
		return coreDao.findTransactionSummaryByPaymentType(DateUtil.getMinimumDateTime(date), DateUtil.getMaximumDateTime(date));
	}
	
	public ResultList<Transaction> searchTransaction(Date date, Integer stationId, String code, Integer start, Integer end){
		return coreDao.searchTransaction(DateUtil.getMinimumDateTime(date), DateUtil.getMaximumDateTime(date), stationId, code, start, end);
	}
	
	public Store getStore(String code){
		return coreDao.getStore(code);
	}
	
	public ResultList<TransferOrder> searchTransferOrder(com.swinarta.sunflower.core.model.TransferOrder.Status status, Integer start, Integer end){
		return coreDao.searchTransferOrder(status, start, end);
	}
	
	public List<TransferOrderDetail> getTransferOrderDetails(Integer transferId){
		return coreDao.getTransferOrderDetails(transferId);
	}
	
	public TransferOrderDetail getTransferOrderDetail(Integer detailId){
		return coreDao.getTransferOrderDetail(detailId);
	}

	public TransferOrder updateProcessTransferOrder(Integer transferId) throws Exception{
		TransferOrder transfer = universalDao.get(TransferOrder.class, transferId);
		transfer.setStatus(com.swinarta.sunflower.core.model.TransferOrder.Status.PROCESSED);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User actor = (User)auth.getPrincipal();

		Store fromStore = getStore(transfer.getFromStoreCode());
		Store currStore = getCurrentStore();
		
		if(fromStore.getCode().equalsIgnoreCase(currStore.getCode())){
			updateStocks(coreDao.getTransferOrderDetails(transferId), StockUpdateReason.TR_OUT, false);
		}else{
			String url = fromStore.getRemoteURL() + "/transferstock/out/" + transferId + "/" + actor.getId();
			try{
				restTemplate.delete(url);
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}			
		}

		return (TransferOrder) universalDao.save(transfer);
	}

	public TransferOrder updateCancelProcessTransferOrder(Integer transferId) throws Exception{
		TransferOrder transfer = universalDao.get(TransferOrder.class, transferId);
		transfer.setStatus(com.swinarta.sunflower.core.model.TransferOrder.Status.CANCELLED);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User actor = (User)auth.getPrincipal();

		Store fromStore = getStore(transfer.getFromStoreCode());
		Store currStore = getCurrentStore();

		if(fromStore.getCode().equalsIgnoreCase(currStore.getCode())){
			updateStocks(coreDao.getTransferOrderDetails(transferId), StockUpdateReason.TR_OUT_C, true);
		}else{
			String url = fromStore.getRemoteURL() + "/transferstock/cancelout/" + transferId + "/" + actor.getId();
			try{
				restTemplate.delete(url);
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}			
		}

		return (TransferOrder) universalDao.save(transfer);
	}

	public TransferOrder updateCancelNewTransferOrder(Integer transferId) throws Exception{
		TransferOrder transfer = universalDao.get(TransferOrder.class, transferId);
		transfer.setStatus(com.swinarta.sunflower.core.model.TransferOrder.Status.CANCELLED);
		return (TransferOrder) universalDao.save(transfer);
	}

	public TransferOrder updateCompletedTransferOrder(Integer transferId) throws Exception{
		TransferOrder transfer = universalDao.get(TransferOrder.class, transferId);
		transfer.setStatus(com.swinarta.sunflower.core.model.TransferOrder.Status.COMPLETED);
				
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User actor = (User)auth.getPrincipal();
		
		Store toStore = getStore(transfer.getToStoreCode());
		Store currStore = getCurrentStore();
		
		if(toStore.getCode().equalsIgnoreCase(currStore.getCode())){
			updateStocks(coreDao.getTransferOrderDetails(transferId), StockUpdateReason.TR_IN, true);
		}else{
			String url = toStore.getRemoteURL() + "/transferstock/in/" + transferId + "/" + actor.getId();
			try{
				restTemplate.delete(url);
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}			
		}
				
		return (TransferOrder) universalDao.save(transfer);
	}
	
	private void updateStocks(List<TransferOrderDetail> transferDetails, StockUpdateReason reason, boolean toAdd) throws Exception{
		Integer currStoreId = Integer.parseInt(properties.getProperty("curr.store.id"));
		
		for (TransferOrderDetail detail : transferDetails) {
			Integer measurementQty;
			Set<ProductMeasurement> pms =  detail.getProduct().getProductMeasurement();
			if(pms.isEmpty()){
				measurementQty = detail.getProduct().getBuying().getMeasurement().getDefaultQty();
			}else{
				measurementQty = pms.iterator().next().getOverrideQty();
			}
			
			int add = 1;
			
			if(!toAdd){
				add = -1;
			}
			
			Float transferQty = detail.getQty();
			Float qtyToAdd = add * transferQty * measurementQty;
						
			Set<Stock> stocks = detail.getProduct().getStock();
			Stock stock;
			if(stocks.isEmpty()){
				stock = new Stock();
				stock.setCurrent(qtyToAdd);
				stock.setStoreId(currStoreId);
				stock.setProduct(detail.getProduct());
			}else{
				stock = stocks.iterator().next();
				stock.setCurrent(stock.getCurrent() + qtyToAdd);
			}
			stock.setLastUpdateReason(reason);
			universalDao.save(stock);
			
		}
	}

	public void updateStockRemote(Integer transferId, StockUpdateReason reason, boolean toAdd) throws Exception{
		universalDao.get(TransferOrder.class, transferId);
		updateStocks(coreDao.getTransferOrderDetails(transferId), reason, toAdd);		
	}
	
	public Store getCurrentStore(){
		Integer currStoreId = Integer.parseInt(properties.getProperty("curr.store.id"));
		return universalDao.get(Store.class, currStoreId);
	}

}
